symbols = {}

class mAST:
    action = None
    params = None

    def __init__(self, action=None, params=None):
        self.action = action
        self.params = params
        

    def execute(self):
        result = None
        
        if self.action == 'print':
            print(' '.join(str(mAST.resolve(x)) for x in list(self.params)))
        elif self.action == 'assign':
            result = symbols[self.params[0]] = mAST.resolve(self.params[1])
            
        elif self.action == 'get':
            result = symbols.get(self.params[0], 0)
        
        elif self.action == 'condition':
            # print 'i am in codition:',mAST.resolve(self.params[:])
            if mAST.resolve(self.params[0]):
                result = mAST.resolve(self.params[1])
                # print 'ture:',result
            else:
                result = mAST.resolve(self.params[2])
                print 'false:',result

            

        elif self.action=='len':
            #
            result = len(mAST.resolve(self.params[0]))
            #print result
            #print 'i am in len action'
            #result = symbols.get(self.params[0], 0)
            
        elif self.action=='brac':
            list_p =mAST.resolve(self.params[0])
            result=list_p[self.params[1]]
        
        elif self.action=='subscript':
            #print self.params[:]
            list_temp =mAST.resolve(self.params[0])
            #print list_p
            result=list_temp[self.params[1]:self.params[2]]
        
        elif self.action=='chop':
            list_temp =mAST.resolve(self.params[0])
            result=int(list_temp)
        
        elif self.action=='str':
            list_temp =mAST.resolve(self.params[0])
            result=str(list_temp)

        elif self.action=='input':
            list_temp =mAST.resolve(self.params[0]) 
            if list_temp==None:
                result=input('>')
            else:
                result=input(list_temp)
        elif self.action=='while':
            print('while ::: )',mAST.resolve(self.params[:]))





            

        return result

    @staticmethod
    def isADelayedAction(x=None):
        return ('x' is not None and isinstance(x, mAST))

    @staticmethod
    def resolve(x):
        
        if not mAST.isADelayedAction(x):
           
            return x
        else:
            return x.execute()


from re import escape
import ply.lex as lex


states = (
    ('string','inclusive'),
    
)
#list of basic token names
tokens=(
    'NUMBER',
    'ID',
    'SEMI',
    'STRING',
)
#reserved tokens
reserved ={
    'print': 'PRINT',
    'input':'INPUT',
    'while':'WHILE',
    'range': 'RANGE',
    'if':    'IF',
    'else':  'ELSE',
    'for':   'FOR',
    'in':    'IN',
    'and':   'AND',
    'or':    'OR',
    'true': 'TRUE',
    'false': 'FALSE',
}

#list of single character literals
#sc =single  char
specials_sc = {
    '+':    'ADD',
    '-':    'REM',
    '*':    'MUL',
    '/':    'DIV',
    '%':    'MOD',
    '^':    'POW',
    '=':    'ASSIGN',
    '<':    'LT',
    '>':    'GT',
    '(':    'LPAREN',
    ')':    'RPAREN',
    '[':    'LBRACKET',
    ']':    'RBRACKET',
    ',':    'COMMA',
    ':':    'COLON',
}

#list  of multiple character literals
#ms = multiple char
specials_mc={
    '<>':   'NE',
    ':=':   'AP',   # Appointment
}

# precedence tokens
precedence = (
    ('nonassoc', 'LOOP_INSTR'), # Nonassociative operators
    ('nonassoc', 'IFX', 'SEMI', 'FOR'),
    ('left', 'CONDLIST'),
    ('left', 'LT', 'LE', 'GT', 'GE', 'EQ', 'NE'),
    ('left', 'REM', 'ADD'),
    ('left', 'MUL', 'DIV', 'MOD'),
    ('left', 'POW'),
    ('right', 'NEGATE'),
)


#All tokens
tokens = list(tokens) + list(reserved.values()) \
    + list(specials_mc.values()) + list(specials_sc.values())


# Opening of first comment
# def t_LCOMM(t):
#     r'\/\*'
#     t.lexer.comment_level = 1
#     t.lexer.begin('comment')


# # Opening of first nested comment
# def t_comment_LCOMM(t):
#     r'\/\*'
#     t.lexer.comment_level += 1


# # Ignore everything inside a comment
# t_comment_ignore_contents = r'[\s\S]'
# # Just for removing PLY's warning
# t_comment_ignore = ''


# # Closing a comment
# def t_comment_RCOMM(t):
#     r'\*\/'
#     t.lexer.comment_level -= 1

#     # If it was the most outer comment closing
#     if t.lexer.comment_level == 0:
#         t.lexer.begin('INITIAL')




def t_ID(t):
    #check for  reserved words
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value.lower(), 'ID')

def t_NUMBER(t):
    #rule for number
    r'(0|[1-9]\d*)'
    t.value = int(t.value)
    return t

def t_FLOAT(t):
    r'\d+\.\d*(e-?\d+)?'
    t.value = float(t.value)
    return t


def t_SEMI(t):
    #Role for end line and (';')
    r'\n|;+'
    t.lexer.lineno += len(t.value)
    t.type = 'SEMI'
    # t.value = ';'
    return t

def t_STRING(t):
    r'[\"\']'
    t.lexer.begin('string')
    t.lexer.str_start = t.lexer.lexpos
    t.lexer.str_marker = t.value


def t_string_chars(t):
    r'[^"\'\n]+'


def t_string_newline(t):
    r'\n+'
    print("Incorrectly terminated string %s" % t.lexer.lexdata[t.lexer.str_start:t.lexer.lexpos - 1])
    t.lexer.skip(1)


def t_string_end(t):
    r'[\"\']'

    if t.lexer.str_marker == t.value:
        t.type = 'STRING'
        t.value = t.lexer.lexdata[t.lexer.str_start:t.lexer.lexpos - 1]
        t.lexer.begin('INITIAL')
        return t


# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'



def find_column(t):
    last_cr = t.lexer.lexdata.rfind('\n', 0, t.lexpos)
    if last_cr < 0:
        last_cr = 0
    column = (t.lexpos - last_cr) + 1
    return column

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

# Build the lexer
lexer = lex.lex()
test="while if print input"

lexer.input(test)
# Tokenize
while True:
    tok = lexer.token()
    if not tok: 
        break      # No more input
    print(tok)





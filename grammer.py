import ply.yacc as yacc
import  viruxlex
from mAST import mAST

#import  some required globals from tokenizer
tokens=viruxlex.tokens
precedence=viruxlex.precedence
astList = []
#program start point

def p_program(p):
    """s : '{' function '}'"""
    p[0] = astList

    


def p_function(p):
    """
    function : function statement ';'
             | function line_statement
             | empty
    """
    if len(p) > 2:
        astList.append(p[2])
        p[0] = p[2]

def p_statement_none(p):
    """line_statement : """
    pass

def p_statement_expr(p):
    """line_statement : expression ';'"""
    p[0] = p[1]

def p_statement_assign(p):
    """ 
    line_statement : ID '=' expression ';'
                   | ID '=' condition_list ';'

    """
    p[0] = mAST(action='assign', params=[p[1], p[3]])

def p_statement_print(p):
    """ statement : PRINT '(' expr_list ')' """
    p[0]= mAST(action='print', params=p[3])
    

#role IF
# def p_statement_cond_postfix_else(p):
#     """line_statement : statement IF condition_list ELSE statement ';' """
    
#     p[0] = mAST(action='condition', params=[p[3], p[1], p[5]])


def p_ifassign(p):
    """if_assign : ID '=' expression"""
    p[0] = [p[1], p[3]]


# def p_statement_cond_postfix_assign(p):
#     """line_statement : if_assign IF condition_list ELSE expression ';' """
    
#     p[0] = mAST(action='assign', params=[
#         p[1][0], mAST(action='condition', params=[p[3], p[1][1], p[5]])
#     ])


def p_statement_cond(p):
    '''
    line_statement : IF condition_list '{'  statement ';' '}' ELSE '{' statement ';' '}' %prec IFX               
    '''
    p[0] = mAST(action='condition', params=[p[2], p[4],p[9]])
    #print p[3]

def p_statement_loop(p):
    '''
    line_statement : WHILE condition_list '{'  statement ';' '}'               
    '''
    p[0] = mAST(action='while', params=[p[2], p[4]])


def p_expression_list(p):
    '''
    expr_list : expression
              | expr_list ',' expression
    '''
    
    if len(p) <= 3:
        p[0] = [p[1]]
    else:
        p[0] = p[1] + [p[3]]

def p_condition_list(p):
    """
    condition_list : expression %prec CONDLIST
                   
    """
    #| condition_list AND expression | condition_list OR expression
                   
    if len(p)>2:
        p[0]=2
    else:
        p[0]=p[1]

def p_condition_parens(p):
    """
    condition_list : '(' condition_list ')' """
    p[0]=p[2]



def p_expression_bool_true(p):
    'expression : TRUE'
    p[0] = True

def p_expression_bool_false(p):
    'expression : FALSE'
    p[0] = False

def p_expression_string(p):
    'expression : STRING'
    p[0]=p[1]


def p_expression_var(p):
    'expression : ID'
    p[0] = mAST(action='get', params=[p[1]])

def p_expression_int(p):
    'expression : INT'
    p[0]=int(p[1])

def p_expression_chop(p):
    """expression : CHOP '(' expression ')'  """
    p[0] = mAST(action='chop', params=[p[3]])
    #print (p[0])

def p_str(p):
    """expression : STR '(' expression ')'  """
    p[0] = mAST(action='str', params=[p[3]])


def p_expression_float(p):
    'expression : FLOAT'
    p[0]=float(p[1])

def p_expression_str_index(p):
    """expression : '[' expr_list ']'  """
    p[0]=list(p[2])
    #print 'array :',p[0][2]

def p_expression_str_subscript(p):
    """expression : expression '[' expression ':' expression ']' """
    p[0]= mAST(action='subscript', params=[p[1],p[3],p[5]])



def p_expression_brac(p):
    """expression : expression '[' expression ']' """
    
    p[0]= mAST(action='brac', params=[p[1],p[3]])
    print p[0]

def p_range(p):
    """range : RANGE '(' expr_list ')' ';'"""
    
    p[0] = list(range(p[3][0], p[3][1]))
    

def p_expression_binop(p):
    """
    expression : expression '+' expression
               | expression '-' expression
               | expression '*' expression
               | expression '/' expression
               | expression '%' expression
               | expression '^' expression
               | expression '>' expression
               | expression GE expression
               | expression '<' expression
               | expression LE expression
               | expression '=' expression
               | expression NOTEQUAL expression
   """ 
  
    a=p[1]
    b=p[3]
    op=p[2]

    result = {
                '+':  lambda a, b: a + b,
                '-':  lambda a, b: a - b,
                '*':  lambda a, b: a * b,
                '/':  lambda a, b: a / b,
                '%':  lambda a, b: a % b,
                '^': lambda a, b: a ** b,
                '>':  lambda a, b: (a > b),
                '>=': lambda a, b: (a >= b),
                '<':  lambda a, b: (a < b),
                '<=': lambda a, b: (a <= b),
                '==': lambda a, b: (a == b),
                '!=': lambda a, b: (a != b),
            }[op](a,b)
    p[0]=result

def p_expression_parens(p):
    """expression : '(' expression ')'"""
    p[0]=p[2]

def p_expression_uminus(p):
    """expression : '-' expression %prec NEGATE"""
    
    p[0]=(-1*p[2])


def p_len(p):
    """expression : LEN '(' expression ')'  """
    p[0]= mAST(action='len', params=[p[3]])
    
def p_input(p):
    """ expression : INPUT '(' expr_or_empty ')' """
    p[0]= mAST(action='input', params=[p[3]])


def p_expr_or_empty(p):
    """expr_or_empty : expression
                     |
    """
    if len(p) == 2:
        p[0] = p[1]
    
    

def p_error(p):
    print("Syntax error in input! :))))", p)


def p_empty(p):
    'empty :'
    pass



parser=yacc.yacc()
s="""{print("test");
print("test");
print("test","tesy");
print("test");
print("test");
x;
print(x);}"""
s2=""" 
{
//y=2.4;
//print(y);
//x=to_int(1.24);;
//range ( 1, 4 ) ;
x= [1,2,3333,4,'mohammad hossein'];
//len(x);;
y=2+23+(1+2+2);
print ('x=',x[3:5],'y= ',y);
print(len(x));
z=len(x);
print('z:::)',z);
print('chop:',chop(1.2));
z=chop(1234.2323);
print('len=',len(x[4]));
print('str:',str(len(x[3:5])));
print('convert float to str:',str(1.432));
//q=input('enter your name:');
//print('input:',q);
temp=545;
temp2=23;
//if temp > temp2 {print('iam in if');}else{print('iam in else');}
test=14;
test1=11;
print(test);
temp=test>test1;
print(temp);

print('test::::',test);
if test<test1{print('ok');}else{print('no');}
while(temp){print(adad);}

}
"""

for x in parser.parse(s2):
    
    mAST.resolve(x)
#result = parser.parse(s2)
#print(result)
# while True:
#    try:
#        s = raw_input('virux > ')
#    except EOFError:
#        break
#    if not s: continue
#    result = parser.parse(s)
#    print(result)




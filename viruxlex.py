import  ply.lex as lex


states = (
    ('string', 'inclusive'),
)
# states = (
#     ('string','inclusive'),
    
# )
#list of basic token names
tokens=(
    'NUMBER',
    'ID',
    'STRING',
    'FLOAT',
    'INT',
    'ASSIGN',
    'NOTEQUAL',
    'GE',
    'LE',
    'NEWLI',
)
#reserved tokens
reserved ={
    'print': 'PRINT',
    'input':'INPUT',
    'while':'WHILE',
    'range': 'RANGE',
    'if':    'IF',
    'else':  'ELSE',
    'for':   'FOR',
    'in':    'IN',
    'and':   'AND',
    'or':    'OR',
    'true': 'TRUE',
    'false':'FALSE',
    'chop':'CHOP',
    'len'   :'LEN',
    'str'   :'STR',
}

literals = (
    '>',
    '<',
    '=',
    '+',
    '-',
    '*',
    '/',
    '^',
    '{',
    '}',
    '[',
    ']',
    '(',
    ')',
    ':',
    ';',
    ',',
    
)




#All tokens
tokens = list(tokens) + list(reserved.values()) 

t_NOTEQUAL=r'<>'
t_GE=r'>='
t_LE=r'<='
t_ASSIGN=r':='
t_NEWLI=r'\n'
def t_FLOAT(t):
    r'\d+\.\d*(e-?\d+)?'
    t.value = float(t.value)
    return t

def t_INT(t):
    r'\d+'
    t.value = int(t.value)
    return t
def t_STRING(t):
    r'[\"\']'
    t.lexer.begin('string')
    t.lexer.str_start = t.lexer.lexpos
    t.lexer.str_marker = t.value


def t_string_chars(t):
    r'[^"\'\n]+'


def t_string_newline(t):
    r'\n+'
    print("Incorrectly terminated string %s" % t.lexer.lexdata[t.lexer.str_start:t.lexer.lexpos - 1])
    t.lexer.skip(1)


def t_string_end(t):
    r'[\"\']'

    if t.lexer.str_marker == t.value:
        t.type = 'STRING'
        t.value = t.lexer.lexdata[t.lexer.str_start:t.lexer.lexpos - 1]
        t.lexer.begin('INITIAL')
        return t




def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_ID(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'ID')    # Check for reserved words
    return t
#role for comment  //statment
def t_COMMENT(t):
    r'\/\/+.*'
    pass


# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


# precedence tokens
precedence = (
    #('nonassoc', 'LOOP_INSTR'), # Nonassociative operators
    ('nonassoc', 'IFX', ';', 'FOR'),
    ('left', 'CONDLIST'),
    ('left', '<', 'LE', '>', 'GE', '=', 'NOTEQUAL'),
    ('left', '-', '+'),
    ('left', '*', '/', '%'),
    ('left', '^'),
    ('right', 'NEGATE'),
)

#build lex and run for text  
lexer = lex.lex()
# test=""" while (n = "") {
#         n := input("Enter number of fibonacci items: ");
#         n := to_int(n);

#         //1+1 
#     }"""

# lexer.input(test)
# # Tokenize
# while True:
#     tok = lexer.token()
#     if not tok: 
#         break      # No more input
#     print(tok)
